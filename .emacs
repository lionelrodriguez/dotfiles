(setq user-full-name "Lionel Rodriguez")
(setq user-mail-address "lionelrodriguez@disroot.org")

;; Set emacs root
(defvar emacs-root
  (eval-when-compile
    (if (or (eq system-type 'cygwin)
            (eq system-type 'gnu/linux)
            (eq system-type 'linux)
            (eq system-type 'darwin))
        "~/.emacs.d/"
        "%USER%/.emacs.d/"))
  "Path to where EMACS configuration root is.")

;; Set main emacs directory
(setq init-dir "~/.emacs.d/")

(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

;; Package Section
(require 'package)

;; Repositories Section
;; Define repositories variables
(defvar gnu '("gnu" . "https://elpa.gnu.org/packages/"))
(defvar melpa '("melpa" . "https://melpa.org/packages/"))
(defvar melpa-stable '("melpa-stable" . "https://stable.melpa.org/packages/"))
(defvar org-elpa '("org" . "http://orgmode.org/elpa/"))

;; Add marmalade to package repos
(setq package-archives nil)
(add-to-list 'package-archives melpa-stable t)
(add-to-list 'package-archives melpa t)
(add-to-list 'package-archives gnu t)
(add-to-list 'package-archives org-elpa t)
(add-to-list 'load-path "/usr/share/emacs/site-lisp/ess")
(load "ess-autoloads")
(require 'ess-r-mode)

;;(debug-on-entry 'package-initialize)
(package-initialize)
(package-refresh-contents)
(package-install 'use-package)

(unless (and (file-exists-p (concat init-dir "elpa/archives/gnu"))
             (file-exists-p (concat init-dir "elpa/archives/melpa"))
             (file-exists-p (concat init-dir "elpa/archives/melpa-stable")))
  (package-refresh-contents))
;; End Repositories Section

;; Packages & Extensions Section
(defun packages-install (&rest packages)
  (message "running packages-install")
  (mapc (lambda (package)
          (let ((name (car package))
                (repo (cdr package)))
            (when (not (package-installed-p name))
              (let ((package-archives (list repo)))
                (package-initialize)
                (package-install name)))))
        packages)
  (package-initialize)
  (delete-other-windows))

;; Install extensions if they're missing
(defun init--install-packages ()
  (message "Lets install some packages")
  (packages-install
   ;; Since use-package this is the only entry here
   ;; ALWAYS try to use use-package!
   (cons 'use-package melpa)
   ))

(condition-case nil
   ::(init--install-packages)
  (error
   (package-refresh-contents)
   (init--install-packages)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(pdf-tools undo-tree impatient-mode evil nyan-mode neotree emojify emojify-logos outshine powerline spaceline spaceline-all-the-icons f mode-icons darcula-theme command-log-mode nlinum-hl swiper web-mode magit-gitflow magit company htmlize markdown-mode all-the-icons ace-jump-mode ace-window which-key ivy-hydra counsel-projectile counsel diminish use-package auctex auto-complete-auctex auctex-latexmk unpdf latex-preview-pane jedi)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

 (use-package diminish
  :ensure t)

(use-package hydra
  :ensure t)

(global-set-key (kbd "C-x k") 'kill-this-buffer)

(use-package counsel
    :ensure t
    :bind
    (("M-x" . counsel-M-x)
     ("M-y" . counsel-yank-pop)
     :map ivy-minibuffer-map
     ("M-y" . ivy-next-line)))

 (use-package swiper
     :pin melpa-stable
     :diminish ivy-mode
     :ensure t
     :bind*
     (("C-s" . swiper)
      ("C-c C-r" . ivy-resume)
      ("C-x C-f" . counsel-find-file)
      ("C-c h f" . counsel-describe-function)
      ("C-c h v" . counsel-describe-variable)
      ("C-c i u" . counsel-unicode-char)
      ("M-i" . counsel-imenu)
      ("C-c g" . counsel-git)
      ("C-c j" . counsel-git-grep)
      ("C-c k" . counsel-ag)
;;      ("C-c l" . scounsel-locate)
)
   :config
     (progn
       (ivy-mode 1)
       (setq ivy-use-virtual-buffers t)
       (define-key read-expression-map (kbd "C-r") #'counsel-expression-history)
       (ivy-set-actions
        'counsel-find-file
        '(("d" (lambda (x) (delete-file (expand-file-name x)))
           "delete"
           )))
       (ivy-set-actions
        'ivy-switch-buffer
        '(("k"
           (lambda (x)
             (kill-buffer x)
             (ivy--reset-state ivy-last))
           "kill")
          ("j"
           ivy--switch-buffer-other-window-action
           "other window")))))

  (use-package counsel-projectile
    :ensure t
    :config
    (counsel-projectile-mode))

  (use-package ivy-hydra :ensure t)
  
  (setq ns-pop-up-frames nil)

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil)))
(setq mouse-wheel-progressive-speed nil)
(set-default 'cursor-type 'hbar)

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config
  (which-key-mode))

(if (or
     (eq system-type 'darwin)
     (eq system-type 'berkeley-unix))
    (setq system-name (car (split-string system-name "\\."))))

(setenv "PATH" (concat "/usr/local/bin:" (getenv "PATH")))
(push "/usr/local/bin" exec-path)

;; Set Java Home Directory
;;(setenv "JAVA_HOME" "/usr/lib/jvm/default")

;; Backup settings
(defvar --backup-directory (concat init-dir "backups"))

(if (not (file-exists-p --backup-directory))
    (make-directory --backup-directory t))

(setq backup-directory-alist `(("." . ,--backup-directory)))
(setq make-backup-files t               ; backup of a file the first time it is saved.
      backup-by-copying t               ; don't clobber symlinks
      version-control t                 ; version numbers for backup files
      delete-old-versions t             ; delete excess backup files silently
      delete-by-moving-to-trash t
      kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
      kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
      auto-save-default t               ; auto-save every buffer that visits a file
      auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
      auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
      )
  (setq delete-by-moving-to-trash t
        trash-directory "~/.Trash/emacs")

  (setq backup-directory-alist `(("." . ,(expand-file-name
                                          (concat init-dir "backups")))))
                                          
(use-package ace-window
  :ensure t
  :config
  (global-set-key (kbd "C-x o") 'ace-window))

  ;;Misc stuff
(tool-bar-mode -1)
(menu-bar-mode 0)
(column-number-mode 1)
(line-number-mode 1)
(ido-mode 1)
(global-linum-mode 1)
(mouse-wheel-mode t)

(setq inhibit-startup-message t)

(defun iwb ()
   "indent whole buffer"
   (interactive)
   (delete-trailing-whitespace)
   (indent-region (point-min) (point-max) nil)
   (untabify (point-min) (point-max)))

(global-set-key (kbd "C-c n") 'iwb)

(electric-pair-mode t)

(set-frame-font "hack 10" nil t)

(use-package doom-themes
    :ensure t
    :config
    (doom-themes-visual-bell-config)
    (doom-themes-neotree-config)
    (setq doom-themes-treemacs-theme "doom-colors")
    (doom-themes-treemacs-config)
    (load-theme 'doom-city-lights t))
    

;; Transparency
;;emacs transparency
(set-frame-parameter (selected-frame) 'alpha '(85 60))
(add-to-list 'default-frame-alist '(alpha 85 60))

(eval-when-compile (require 'cl))
(defun toggle-transparency ()
  (interactive)
  (if (/=
       (cadr (frame-parameter nil 'alpha))
       100)
      (set-frame-parameter nil 'alpha '(100 100))
    (set-frame-parameter nil 'alpha '(90 60))))
(global-set-key (kbd "C-c ^") 'toggle-transparency)

;; Set transparency of emacs
(defun transparency (arg &optional active)
  "Sets the transparency of the frame window. 0=transparent/100=opaque"
  (interactive "nEnter alpha value (1-100): \np")
  (let* ((elt (assoc 'alpha default-frame-alist))
         (old (frame-parameter nil 'alpha))
         (new (cond ((atom old)     `(,arg ,arg))
                    ((eql 1 active) `(,arg ,(cadr old)))
                    (t              `(,(car old) ,arg)))))
    (if elt (setcdr elt new) (push `(alpha ,@new) default-frame-alist))
    (set-frame-parameter nil 'alpha new)))
(global-set-key (kbd "C-c %") 'transparency)

(use-package command-log-mode
  :ensure t)

(defun live-coding ()
  (interactive)
  (set-face-attribute 'default nil :font "Inconsolata-13")
  (add-hook 'prog-mode-hook 'command-log-mode)
  ;;(add-hook 'prog-mode-hook (lambda () (focus-mode 1)))
  )

(defun normal-coding ()
  (interactive)
  (set-face-attribute 'default nil :font "Inconsolata-11")
  (add-hook 'prog-mode-hook 'command-log-mode)
  ;;(add-hook 'prog-mode-hook (lambda () (focus-mode 1)))
  )

(eval-after-load "org-indent" '(diminish 'org-indent-mode))

;;Have the ability to use some amazing font icons
(use-package all-the-icons
  :ensure t)

;; Bell deactivate
(defun my-bell-function ())
(setq ring-bell-function 'my-bell-function)
(setq visible-bell nil)

;; Web edit files
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package htmlize
  :ensure t)

(global-prettify-symbols-mode 1)

(use-package company
  :ensure t
  :bind (("C-c /". company-complete))
  :config
  (global-company-mode)
  )
  
;; Git configuration  
(use-package magit
  :ensure t
  :bind (("C-c m" . magit-status)))

(use-package magit-gitflow
  :ensure t
  :config
  (add-hook 'magit-mode-hook 'turn-on-magit-gitflow))
  
(use-package web-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.jsp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.xhtml?\\'" . web-mode))

  (defun my-web-mode-hook ()
    "Hooks for Web mode."
    (setq web-mode-enable-auto-closing t)
    (setq web-mode-enable-auto-quoting t)
    (setq web-mode-markup-indent-offset 2))

  (add-hook 'web-mode-hook  'my-web-mode-hook))

(use-package less-css-mode
  :ensure t)

;; Outlining
(use-package dash
  :ensure t)

(use-package outshine
  :ensure t
  :config
  (add-hook 'prog-mode 'outline-minor-mode)
  (add-hook 'outline-minor-mode 'outshine-mode)
)

;; Mode Line
(use-package mode-icons
  :ensure t
  :config
  (mode-icons-mode t)
)

(use-package f
  :ensure t)
  
(use-package projectile
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'projectile-mode))

;;(use-package nyan-mode
;;  :ensure t
;;  :config
;;  (nyan-mode))

;; NeoTree configuration section
(use-package neotree 
  :ensure t)

;; (global-set-key [f8] 'neotree-toggle)
(setq neo-smart-open t)
(setq projectile-switch-project-action 'neotree-projectile-action)

  (defun neotree-project-dir ()
    "Open NeoTree using the git root."
    (interactive)
    (let ((project-dir (projectile-project-root))
          (file-name (buffer-file-name)))
      (neotree-toggle)
      (if project-dir
          (if (neo-global--window-exists-p)
              (progn
                (neotree-dir project-dir)
                (neotree-find file-name)))
        (message "Could not find git project root."))))

  (global-set-key [f8] 'neotree-project-dir)

(use-package evil
  :ensure t
  :config
  (evil-mode 1)
  )
  
  (use-package impatient-mode
  :ensure t)

(defun markdown-html (buffer)
  (princ (with-current-buffer buffer
    (format "<!DOCTYPE html><html><title>Impatient Markdown</title><xmp theme=\"united\" style=\"display:none;\"> %s  </xmp><script src=\"http://strapdownjs.com/v/0.2/strapdown.js\"></script></html>" (buffer-substring-no-properties (point-min) (point-max))))
         (current-buffer)))

(defun markdown-preview-document ()
  (interactive)
  (impatient-mode 1)
  (setq imp-user-filter #'markdown-html)
  (cl-incf imp-last-state)
  (imp--notify-clients))
  
  ;; Use LaTEX pane
(use-package latex-preview-pane
  :ensure t)

;; preview side-by-side pane
(latex-preview-pane-enable)

; (autoload 'latex-math-preview-expression "latex-math-preview" nil t)
; (autoload 'latex-math-preview-insert-symbol "latex-math-preview" nil t)
; (autoload 'latex-math-preview-save-image-file "latex-math-preview" nil t)
; (autoload 'latex-math-preview-beamer-frame "latex-math-preview" nil t)

; basic configuration
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq TeX-PDF-mode t)
(setq-default TeX-master t)
(add-hook 'LaTeX-mode-hook 'auto-fill-mode)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'linum-mode)

; start reftex-mode
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

; spell checking
(setq ispell-program-name "aspell")
(setq ispell-dictionary "english")
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-buffer)

; custome LaTex command with -shell-escape option.
; useful for converting eps to pdf
(eval-after-load "tex"
  '(add-to-list 'TeX-command-list
                '("LaTeX" "%`%l -shell-escape  %(mode)%' %t"
                  TeX-run-command nil t)))

; always start the server for inverse search
(setq TeX-source-correlate-mode t)
(setq-default TeX-source-correlate-start-server t)

; set preview programs
(setq TeX-view-program-list
      '(("SumatraPDF" "SumatraPDF.exe %o") ; windows
        ("Gsview" "gsview32.exe %o") ; windows
        ("Okular" "okular --unique %o")
        ("Evince" "evince --page-index=%(outpage) %o")
        ("Firefox" "firefox %o")
        ))

; System specific settings
      (setq TeX-view-program-selection
            (quote (((output-dvi style-pstricks) "dvips and gv")
                    (output-dvi "xdvi")
                    (output-pdf "Evince")
                    (output-html "xdg-open"))))

(provide 'latex-settings)

(add-hook 'LaTeX-mode-hook '(lambda () 
		      (progn
			(turn-on-reftex)
			(LaTeX-math-mode  1)
			(flyspell-mode    1)
			(auto-fill-mode   1)
			(setq fill-column 95)
			(parenthesis-register-keys "{([$" LaTeX-mode-map)
			(define-key LaTeX-mode-map (kbd "C-#") 'jb/wc-latex)
			(outline-minor-mode t)
			(setq latex-mode-map LaTeX-mode-map)
			(yas/reload-all)
			(add-to-list 'TeX-command-list 
				     '("images" "utils.py svg2pdf"
				       TeX-run-command nil t))
			(add-to-list 'TeX-command-list 
				     '("make" "latexmk -pdf %s"
				       TeX-run-TeX nil t
				       :help "Run Latexmk on file")))))
			       
(use-package spaceline :ensure t
  :config
  (setq-default mode-line-format '("%e" (:eval (spaceline-ml-main)))))

(use-package spaceline-config :ensure spaceline
  :config
 (spaceline-helm-mode 1)
  (spaceline-emacs-theme))

;(use-package spaceline
;  :ensure t)

(setq-default
 powerline-height 10
 powerline-default-separator 'wave
 spaceline-flycheck-bullet "❖ %s"
 spaceline-separator-dir-left '(right . right)
 spaceline-separator-dir-right '(left . left))

(add-hook 'python-mode-hook 'jedi:setup)
(autoload 'jedi:setup "jedi" nil t) 
(setq jedi:complete-on-dot t)
