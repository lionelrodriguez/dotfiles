! Base colors
*.foreground:  #E1E1E1
*.background:  #1c2023
*.cursorColor: #778899

*.color0:  #1c2023
*.color1:  #bf616a
*.color2:  #a3be8c
*.color3:  #ebcb8b
*.color4:  #8fa1b3
*.color5:  #b48ead
*.color6:  #96b5b4
*.color7:  #E1E1E1
*.color8:  #919ba0
*.color9:  #bf616a
*.color10: #a3be8c
*.color11: #ebcb8b
*.color12: #8fa1b3
*.color13: #b48ead
*.color14: #96b5b4
*.color15: #c0c5ce

! Rofi

!rofi.font: "San Francisco Display 48"
!rofi.line-padding: 2
!rofi.lines: 10
!rofi.eh: 1
!rofi.padding: 400
!rofi.fullscreen: true
!rofi.width: 100
!rofi.bw: 0
!rofi.separator-style: none
!rofi.hide-scrollbar: true
!rofi.kb-cancel: Escape,Alt+F1

!rofi.color-enabled: true
!rofi.color-window: argb:cc2f343f, argb:cc2f343f, argb:cc2f343f
!rofi.color-normal: argb:002f343f, #d8dee8, argb:002f343f, argb:002f343f, #bf616a
!rofi.color-active: #2f343f, #b48ead, #2f343f, #2f343f, #93e5cc
!rofi.color-urgent: #2f343f, #ebcb8b, #2f343f, #2f343f, #ebcb8b

! xterm
xterm*termName: xterm-256color
xterm*font: hack Nerd Fonts
xterm*faceName: hack:size=9
xterm*loginShell: true
xterm*vt100*geometry: 90x34
xterm*saveLines: 2000
xterm*charClass: 33:48,35:48,37:48,43:48,45-47:48,64:48,95:48,126:48
xterm*eightBitInput: false
xterm*selectToClipboard: true

! URxvt
!-------------------------------------------------------------------------------
! Xft settings
!-------------------------------------------------------------------------------

!Xft.dpi:                    96
!Xft.antialias:              false
!Xft.rgba:                   rgb
!Xft.hinting:                true
!Xft.hintstyle:              hintslight

Xft.dpi: 96
Xft.antialias: true      
Xft.hinting: true
Xft.rgba: rgb 
Xft.autohint: false
Xft.hintstyle: hintslight
Xft.lcdfilter: lcddefault

!-------------------------------------------------------------------------------
! URxvt settings
! Colours lifted from Solarized (http://ethanschoonover.com/solarized)
! More info at:
! http://pod.tst.eu/http://cvs.schmorp.de/rxvt-unicode/doc/rxvt.1.pod
!-------------------------------------------------------------------------------

URxvt.depth:                32
URxvt.geometry:             90x30
URxvt.transparent:          true
URxvt.fading:               0
! URxvt.urgentOnBell:         true
! URxvt.visualBell:           true
URxvt.loginShell:           true
URxvt.saveLines:            50
URxvt.internalBorder:       3
URxvt.lineSpace:            0
URxvt.clipboard.autocopy: true
URxvt.keysym.M-c: perl:clipboard:copy
URxvt.keysym.M-v: perl:clipboard:paste

! Fonts
URxvt.allow_bold:           false
/* URxvt.font:                 -*-terminus-medium-r-normal-*-12-120-72-72-c-60-iso8859-1 */
!URxvt*font: xft:Monospace:pixelsize=14
!URxvt*boldFont: xft:Monospace:pixelsize=14

URxvt*font: xft:hack:pixelsize=12
URxvt*boldFont: xft:hack:pixelsize=12

! Fix font space
URxvt*letterSpace: -1

! Scrollbar
URxvt.scrollStyle:          rxvt
URxvt.scrollBar:            false

! Perl extensions
URxvt.perl-ext-common:      default,matcher,selection-to-clipboard,pasta,keyboard-select,clipboard
URxvt.matcher.button:       1
URxvt.urlLauncher:          firefox

! Cursor
URxvt.cursorBlink:          true
URxvt.cursorColor:          #657b83
URxvt.cursorUnderline:      false

! Pointer
URxvt.pointerBlank:         true

!!Source http://github.com/altercation/solarized

*background: #002b36
*foreground: #657b83
!!*fading: 40
*fadeColor: #002b36
*cursorColor: #93a1a1
*pointerColorBackground: #586e75
*pointerColorForeground: #93a1a1

!! black dark/light
*color0: #073642
*color8: #002b36

!! red dark/light
*color1: #dc322f
*color9: #cb4b16

!! green dark/light
*color2: #859900
*color10: #586e75

!! yellow dark/light
*color3: #b58900
*color11: #657b83

!! blue dark/light
*color4: #268bd2
*color12: #839496

!! magenta dark/light
*color5: #d33682
*color13: #6c71c4

!! cyan dark/light
*color6: #2aa198
*color14: #93a1a1

!! white dark/light
*color7: #eee8d5
*color15: #fdf6e3

! UXTerm
UXTerm*termName: xterm-256color
UXTerm*cursorColor: white
UXTerm*VT100.geometry: 90x34
UXTerm*font: hack
UXTerm*faceSize: 9
UXTerm*dynamicColors: true
UXTerm*utf8: 2
UXTerm*eightBitInput: true
UXTerm*saveLines: 10000
UXTerm*scrollKey: true
UXTerm*scrollTtyOutput: false
UXTerm*scrollBar: false
UXTerm*rightScrollBar: false
UXTerm*jumpScroll: true
UXTerm*multiScroll: true
UXTerm*toolBar: false

