# Dotfiles Section

## Description

In this repository throw all my stuff configuration files for other future installation
```
  foo@bar:~$ git init --bare $HOME/.dotfiles
  foo@bar:~$ dotfiles config --local status.showUntrackedFiles no
  foo@bar:~$ echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.bash_profile
  foo@bar:~$ echo "alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'" >> $HOME/.bashrc
```
or

```
  curl -s https://gitlab.com/lionelrodriguez/dotfiles/-/raw/master/.dotfiles.sh | bash
```
