#!/bin/sh

loadkeys la-latin1

show_disk_partitions(){
 lsblk
 blkid -g
 sync
}

create_default_partitions(){
 echo "Creating partitions:"
 parted /dev/${hdd} --script -- mklabel gpt
 parted /dev/${hdd} --script -- mkpart EFI fat32 2048s 513MiB
 parted /dev/${hdd} --script -- set 1 esp on
 parted /dev/${hdd} --script -- mkpart swap linux-swap 513MiB 8704MiB
 parted /dev/${hdd} --script -- mkpart root ext4 8704MiB 16896MiB
 parted /dev/${hdd} --script -- mkpart gnu ext4 16896MiB 82432MiB
 parted /dev/${hdd} --script -- mkpart home ext4 82432MiB 115200MiB
 
 sleep 3
 
 lsblk
 blkid -g
 sync
}

format_partitions(){
 while true; do
     read -p "Format 'Em All?: (y/n)" yn
     case $yn in
         [Yy]* ) do_format_partitions; break;;
         [Nn]* ) exit;;
         * ) echo "Please answer y or n.";;
     esac
 done
}

do_format_partitions(){
 mkfs.fat -F32 /dev/disk/by-partuuid/${EFI_PARTUUID}
 mkfs.ext4 /dev/disk/by-partuuid/${ROOT_PARTUUID}
 mkfs.ext4 /dev/disk/by-partuuid/${HOME_PARTUUID}
 mkfs.ext4 /dev/disk/by-partuuid/${GNU_PARTUUID}
 mkswap /dev/disk/by-partuuid/${SWAP_PARTUUID}

 echo "Activating swap partition:"
 swapon -U $(blkid -s UUID -o value /dev/disk/by-partuuid/${SWAP_PARTUUID})
}

get_partuuid_partitions(){
 echo "Gettng PARTUUID's from partitions..."
 
 blk_name=($(lsblk -o partlabel | sed -e 's/^\s*//' -e '/^$/d' | grep -v PARTLABEL))
 blk_uuid=($(lsblk -o partuuid | sed -e 's/^\s*//' -e '/^$/d' | grep -v PARTUUID))

  for ((i=0; i<${#blk_name[@]}; i++)); do
  echo "First parameter : ${blk_name[$i]}, second parameter : ${blk_uuid[$i]}"
  export ${blk_name[$i]^^}_PARTUUID=${blk_uuid[$i]}
  sleep 1
 done

 env | grep PARTUUID | sort
}
mount_partitions(){
 echo "Mounting partitions..."
 mount -t ext4 -U $(blkid -s UUID -o value /dev/disk/by-partuuid/${ROOT_PARTUUID}) /mnt

 mkdir -p /mnt/boot/efi
 mkdir -p /mnt/home
 mkdir -p /mnt/gnu
 
 mount -t vfat -U $(blkid -s UUID -o value /dev/disk/by-partuuid/${EFI_PARTUUID}) /mnt/boot/efi
 mount -t ext4 -U $(blkid -s UUID -o value /dev/disk/by-partuuid/${HOME_PARTUUID}) /mnt/home 
 mount -t ext4 -U $(blkid -s UUID -o value /dev/disk/by-partuuid/${GNU_PARTUUID}) /mnt/gnu

 mkdir -p /home/f2khllw/guixsd/dotfiles/etc/tor
 touch /home/f2khllw/guixsd/dotfiles/etc/tor/torrc
}

get_config(){
  echo "Gettng config.scm template..."
  mkdir -p /mnt/etc
  curl https://gitlab.com/lionelrodriguez/dotfiles/-/raw/master/guix/config/base/config.scm -o /mnt/etc/config.scm
  
  if [ $? -eq 0 ]; then
    parse_config_scm 
  else
    echo "We are fucked."
    exit -1
  fi
}

parse_config_scm(){
 echo "Inserting UUID's in config.scm..."
 sed -i "s/REPLACE_EFI_UUID/$(blkid -s UUID -o value /dev/disk/by-partuuid/${EFI_PARTUUID})/g" /mnt/etc/config.scm
 sed -i "s/REPLACE_ROOT_UUID/$(blkid -s UUID -o value /dev/disk/by-partuuid/${ROOT_PARTUUID})/g" /mnt/etc/config.scm
 sed -i "s/REPLACE_HOME_UUID/$(blkid -s UUID -o value /dev/disk/by-partuuid/${HOME_PARTUUID})/g" /mnt/etc/config.scm
 sed -i "s/REPLACE_GNU_UUID/$(blkid -s UUID -o value /dev/disk/by-partuuid/${GNU_PARTUUID})/g" /mnt/etc/config.scm
}

start_cow_store(){
 echo "Starting Cow-Store..."
 herd start cow-store /mnt
}

init_installation(){
 echo "Initalizing new system..."
 guix system init /mnt/etc/config.scm /mnt
}

show_disk_partitions
echo ""
read -p "Enter device name: (sda):" hdd
echo ""
create_default_partitions
get_partuuid_partitions
format_partitions
mount_partitions
get_config
start_cow_store
init_installation
