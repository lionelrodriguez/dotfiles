;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules 
    (gnu)
    (gnu system)
    (guix store)
    (gnu packages conky)
    (gnu packages compton)
    (gnu packages cups)
    (gnu packages disk)
    (gnu services dbus)
    (gnu services desktop)
    (gnu services docker)
    (gnu packages dunst)
    (gnu packages emacs)
    (gnu packages fonts)
    (gnu packages libusb)
    (gnu packages mail)
    (gnu packages mpd)
    (gnu packages networking)
    (gnu system nss)
    (gnu packages package-management)
    (gnu packages pdf)
    (gnu services shepherd)
    (gnu packages stalonetray)
    (gnu packages ssh)
    (gnu packages terminals)
    (gnu packages video)
    (gnu packages version-control)
    (srfi srfi-1))


(use-service-modules avahi base dbus cups desktop virtualization web pm nfs dns admin docker sound desktop networking ssh xorg)

(use-package-modules
            admin
            avahi
            admin
            backup
            bash
            certs
            code
            compression
            cpio
            crypto
            cryptsetup
            docker 
            disk
            dns
            emacs
            engineering
            efi
            image-viewers
            file
            fonts
            fontutils
            freedesktop
            gimp
            guile
            gnunet
            gnupg
            inkscape
            kde-frameworks
            libusb
            linux
            mpd
            nano
            nfs
            nss
            ocaml
            password-utils
            package-management
            printers
            python
            pulseaudio 
            readline
            rsync
            scheme
            shells
            shellutils
            screen
            suckless
            terminals
            tor
            xdisorg
            xfce
            xorg
            version-control
            video
            virtualization
            vim
            w3m
            web-browsers 
            wget)

(operating-system
  (locale "en_US.utf8")
  (timezone "America/Argentina/Buenos_Aires")
  (keyboard-layout (keyboard-layout "latam"))
  (host-name "AltoGuixo")
  (users (cons* (user-account
                  (name "f2khllw")
                  (comment "Lionel")
                  (group "users")
                  (home-directory "/home/users/f2khllw")
                  (shell #~(string-append #$bash "/bin/bash"))
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video"
                     "lp" "lpadmin" "avahi" "users" "kvm" "libvirt")))
                    %base-user-accounts))
  (packages
    (append
      (list (specification->package "avahi")
            (specification->package "nss-certs")
            (specification->package "openssh"))
      %base-packages))
  
  (services
    (cons*
            (set-xorg-configuration
                (xorg-configuration
                (keyboard-layout keyboard-layout)))

            (service docker-service-type)

            (service openssh-service-type
                (openssh-configuration(x11-forwarding? #t)
                (openssh openssh-sans-x)
                (port-number 2222)))

            ;(service static-networking-service-type
            ;    (list
            ;        (static-networking 
            ;            (interface "eth0")
            ;            (ip  "192.168.1.182")
            ;            (gateway "192.168.1.1")
            ;            (name-servers '("1.1.1.1")))))

            ;(static-networking-service "eth0" "192.168.1.182"
            ;    #:netmask "255.255.255.0" 
            ;    #:gateway "192.168.1.1"
            ;    #:name-servers '("1.1.1.1"))

            (service cups-service-type
                (cups-configuration
                (web-interface? #t)
                (browsing? #t)
                (default-paper-size "a4")))

            (service libvirt-service-type
                (libvirt-configuration
                (unix-sock-group "libvirt")))

            (service thermald-service-type)
            
            (service slim-service-type (slim-configuration
                                               (display ":0")
                                               (vt "vt0")))
                   (service slim-service-type (slim-configuration
                                               (display ":1")
                                               (vt "vt1")))
                   (remove (lambda (service)
                             (eq? (service-kind service) gdm-service-type)))
            %desktop-services))
            
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (target "/boot/efi")
      (keyboard-layout keyboard-layout)))
  (file-systems
    (cons* (file-system
             (mount-point "/home")
             (device
               (uuid "REPLACE_HOME_UUID"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/gnu")
             (device
               (uuid "REPLACE_GNU_UUID"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/")
             (device
               (uuid "REPLACE_ROOT_UUID"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/boot/efi")
             (device (uuid "REPLACE_EFI_UUID" 'fat32))
             (type "vfat"))
           %base-file-systems)))
