dateMask="$(date +"%Y%m%d%H%M%S")"
backupDirectory="/home/lionel/.backup/chromium/backup"
mkdir -p "$backupDirectory/$dateMask"
tar cjvPf "$backupDirectory/$dateMask/backupChromium-$dateMask.tar.xz" "$HOME/.config/chromium"

if [ $? -eq 0 ]; then
   rm -rfv $HOME/.config/chromium/Default
   rm -rfv $HOME/.config/chromium/*
else
  echo "Ocurrió un error y no se borró una mierda la caché del Chromium."
fi
