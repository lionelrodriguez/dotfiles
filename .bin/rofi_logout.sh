#!/bin/bash

MONITOR=`xrandr | grep connected | grep -v disconnected | cut -d ' ' -f 1`

for window_id in $(bspc query -W); do
    bspc window $window_id -c
done

if [[ $MONITOR == 'DVI-I-1' ]];then
  mpc stop
  killall sxhkd
  killall compton
  killall feh
  killall bash
  killall conky
else
  kill `ps -u $USER --sort=start_time | grep sxhkd | tail -1 | cut -d ' ' -f 1`
  kill `ps -u $USER --sort=start_time | grep compton | tail -1 | cut -d ' ' -f 1`
  kill `ps -u $USER --sort=start_time | grep feh | tail -1 | cut -d ' ' -f 1`
  kill `ps -u $USER --sort=start_time | grep bash | tail -1 | cut -d ' ' -f 1`
  kill `ps -u $USER --sort=start_time | grep conky | tail -2 | head -1 | cut -d ' ' -f 1`
  kill `ps -u $USER --sort=start_time | grep conky | tail -1 | cut -d ' ' -f 1`
fi

bspc quit
