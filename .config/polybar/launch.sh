#!/usr/bin/env bash


# Wait until the processes have been shut down
export MONITOR=`xrandr | grep connected | grep -v disconnected | cut -d ' ' -f 1`

# Terminate already running bar instances

if [[ $MONITOR == 'DVI-I-1' ]]; then
  killall -q polybar
fi

# Wait until the processes have been shut down
#while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

#for m in $(polybar --list-monitors | cut -d":" -f1); do
#	WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') MONITOR=$m polybar --reload mainbar-i3 &
	#WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') MONITOR=$m polybar --reload mainbar-bspwm &
#done

polybar -c $HOME/.config/polybar/config -r mainbar-i3 &

echo "Bars launched..."
